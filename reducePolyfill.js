function reducePolyfill (array, callback, initial) {
    if (array.length === 0 && initial === undefined) {
        throw new Error (`${array} is empty and ${initial} isn't defined`);
    }
    if (typeof callback !== 'function') {
        throw new TypeError(`${callback} is not a function`);
    }

    for (let i = 0; i < array.length; i++) {

        initial = initial === undefined ? array[i]  : callback(initial, array[i]);
    }

    return initial;
}


let arr = [1, 2, 3];

let print = reducePolyfill(arr, (a, b)=>{
    return a + b;
});

console.log(print);